﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace ShowPin.MvcPaging
{
    /// <summary>
    /// 分页扩展
    /// </summary>
    public static class PagingExtensions
    {
        #region IQueryable<T> extensions

        /// <summary>
        /// 执行分页
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">要执行分页的数据源</param>
        /// <param name="currentPage">当前页</param>
        /// <param name="pageSize">每页显示数据量</param>
        /// <returns></returns>
        public static IPagedList<T> ToPagedList<T>(this IQueryable<T> source, int currentPage, int pageSize)
        {
            return new PagedList<T>(source, currentPage, pageSize);
        }

        #endregion

        #region IEnumerable<T> extensions

        /// <summary>
        /// 执行分页
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">要执行分页的数据源</param>
        /// <param name="currentPage">当前页</param>
        /// <param name="pageSize">每页显示数据量</param>
        /// <returns></returns>
        public static IPagedList<T> ToPagedList<T>(this IEnumerable<T> source, int currentPage, int pageSize)
        {
            return new PagedList<T>(source, currentPage, pageSize);
        }

        #endregion

        #region HtmlHelper extensions

        /// <summary>
        /// 分页控件
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="pageSize">每页显示数据量</param>
        /// <param name="currentPage">当前页</param>
        /// <param name="totalItemCount">总数据量</param>
        /// <returns></returns>
        public static Pager Pager(this HtmlHelper htmlHelper, int pageSize, int currentPage, int totalItemCount)
        {
            return new Pager(htmlHelper, pageSize, currentPage, totalItemCount);
        }

        /// <summary>
        /// 分页控件
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="pageSize">每页显示数据量</param>
        /// <param name="currentPage">当前页</param>
        /// <param name="totalItemCount">总数据量</param>
        /// <param name="ajaxOptions">设置ajas参数</param>
        /// <returns></returns>
        public static Pager Pager(this HtmlHelper htmlHelper, int pageSize, int currentPage, int totalItemCount, AjaxOptions ajaxOptions)
        {
            return new Pager(htmlHelper, pageSize, currentPage, totalItemCount).Options(o => o.AjaxOptions(ajaxOptions));
        }

        #endregion
    }
}
