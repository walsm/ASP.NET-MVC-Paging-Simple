﻿using System.Collections.Generic;
using System.Web.Mvc.Ajax;

namespace ShowPin.MvcPaging
{
    /// <summary>
    /// 分页模型
    /// </summary>
    public class PaginationModel
    {
        /// <summary>
        /// 链接列表
        /// </summary>
        public List<PaginationLink> PaginationLinks { get; private set; }

        /// <summary>
        /// 表示用于在 ASP.NET MVC 应用程序中运行 Ajax 脚本的选项设置
        /// </summary>
        public AjaxOptions AjaxOptions { get; internal set; }

        /// <summary>
        /// 分页选项
        /// </summary>
        public PagerOptions Options { get; internal set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        public PaginationModel()
        {
            PaginationLinks = new List<PaginationLink>();
            AjaxOptions = null;
            Options = null;
        }
    }

    /// <summary>
    /// 分页链接
    /// </summary>
    public class PaginationLink
    {
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// 是否当前页
        /// </summary>
        public bool IsCurrent { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 显示文字
        /// </summary>
        public string DisplayText { get; set; }

        /// <summary>
        /// URL
        /// </summary>
        public string Url { get; set; }
    }
}
