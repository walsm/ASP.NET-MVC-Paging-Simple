﻿using System;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Routing;

namespace ShowPin.MvcPaging
{
    /// <summary>
    /// 构造分页选项
    /// </summary>
    public class PagerOptionsBuilder
    {
        private readonly PagerOptions _pagerOptions;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="pagerOptions">分页选项</param>
        public PagerOptionsBuilder(PagerOptions pagerOptions)
        {
            _pagerOptions = pagerOptions;
        }

        /// <summary>
        /// 设置显示页码数 默认为10 不能小于3
        /// </summary>
        /// <param name="pageNumber">显示页码数 默认为10 不能小于3</param>
        /// <returns></returns>
        public PagerOptionsBuilder PageNumber(int pageNumber)
        {
            _pagerOptions.PageNumber = pageNumber < 3 ? 10 : pageNumber;
            return this;
        }

        /// <summary>
        /// 设置路由参数
        /// </summary>
        /// <param name="routeValues">一个对象，包含将作为元素添加到新集合中的属性</param>
        /// <returns></returns>
        public PagerOptionsBuilder RouteValues(object routeValues)
        {
            _pagerOptions.RouteValues = new RouteValueDictionary(routeValues);
            return this;
        }

        /// <summary>
        /// 设置路由名称
        /// </summary>
        /// <param name="routeName">路由名称</param>
        /// <returns></returns>
        public PagerOptionsBuilder RouteName(string routeName)
        {
            _pagerOptions.RouteName = routeName;
            return this;
        }

        /// <summary>
        /// 设置包含元素 HTML 特性的对象
        /// </summary>
        /// <param name="htmlAttributes"> HTML 特性</param>
        /// <returns></returns>
        public PagerOptionsBuilder HtmlAttributes(object htmlAttributes)
        {
            _pagerOptions.HtmlAttributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            return this;
        }

        /// <summary>
        /// 设置程序中运行 Ajax 脚本的选项设置
        /// </summary>
        /// <param name="ajaxOptions">程序中运行 Ajax 脚本的选项设置</param>
        /// <returns></returns>
        internal PagerOptionsBuilder AjaxOptions(AjaxOptions ajaxOptions)
        {
            _pagerOptions.AjaxOptions = ajaxOptions;
            return this;
        }

        /// <summary>
        /// 设置显示模版名
        /// </summary>
        /// <param name="displayTemplate">模版名</param>
        /// <remarks>The view must have a model of IEnumerable&lt;PaginationModel&gt;</remarks>
        /// <returns></returns>
        public PagerOptionsBuilder DisplayTemplate(string displayTemplate)
        {
            _pagerOptions.DisplayTemplate = displayTemplate;
            return this;
        }

        /// <summary>
        /// 设置分页的键 默认分页参数KEY 默认为 page
        /// </summary>
        /// <param name="pageRouteValueKey">默认分页参数KEY 默认为 page</param>
        /// <returns></returns>
        public PagerOptionsBuilder PageRouteValueKey(string pageRouteValueKey)
        {
            if (pageRouteValueKey == null)
            {
                throw new ArgumentException("pageRouteValueKey may not be null", "pageRouteValueKey");
            }
            _pagerOptions.PageRouteValueKey = pageRouteValueKey;
            return this;
        }
    }
}
