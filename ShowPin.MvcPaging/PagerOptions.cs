﻿using System.Web.Mvc.Ajax;
using System.Web.Routing;

namespace ShowPin.MvcPaging
{
    /// <summary>
    /// 分页选项
    /// </summary>
    public class PagerOptions
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public PagerOptions()
        {
            RouteValues = new RouteValueDictionary();
            PageRouteValueKey = "page";
            PageNumber = 10;
        }

        /// <summary>
        /// 包含元素 HTML 特性的对象
        /// </summary>
        public RouteValueDictionary HtmlAttributes { get; set; }

        /// <summary>
        /// 路由参数
        /// </summary>
        public RouteValueDictionary RouteValues { get; internal set; }

        /// <summary>
        /// 路由名称
        /// </summary>
        public string RouteName { get; set; }

        /// <summary>
        /// 默认分页参数KEY 默认为 page
        /// </summary>
        public string PageRouteValueKey { get; set; }

        /// <summary>
        /// 显示页码数 默认10 
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// 自定义显示模版
        /// </summary>
        public string DisplayTemplate { get; internal set; }

        /// <summary>
        /// 程序中运行 Ajax 脚本的选项设置
        /// </summary>
        public AjaxOptions AjaxOptions { get; internal set; }
    }
}
