﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ShowPin.MvcPaging.Demo.Models;

namespace ShowPin.MvcPaging.Demo.Controllers
{
    public class PagingController : Controller
    {
        int _currentPage = 1;
        int _pageSize = 10;
        private readonly string[] _allCategories = { "吃的", "穿的", "用的" };
        private readonly IList<Product> _allProducts = new List<Product>();

        public PagingController()
        {
            InitializeProducts();
        }

        //简单初始化产品集合
        private void InitializeProducts()
        {
            for (var i = 0; i < 122; i++)
            {
                var product = new Product
                {
                    Name = "产品 " + (i + 1)
                };
                var categoryIndex = i % 4;
                if (categoryIndex > 2)
                {
                    categoryIndex = categoryIndex - 3;
                }
                product.Category = _allCategories[categoryIndex];
                _allProducts.Add(product);
            }
        }


        //简单的分页
        public ActionResult Index(int? page = 1, int? size = 10)
        {
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;

            var model = _allProducts.ToPagedList(_currentPage, _pageSize);
            return View(model);
        }

        //根据产品分类搜索分页
        public ActionResult PagingByCategory(string categoryName, int? page = 1, int? size = 10)
        {
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;

            categoryName = categoryName ?? _allCategories[0];
            var model = _allProducts.Where(m => m.Category == categoryName)
                .ToPagedList(_currentPage, _pageSize);

            ViewBag.CategoryName = new SelectList(_allCategories, categoryName);
            ViewBag.CategoryDisplayName = categoryName;
            return View(model);
        }

        //根据多个产品分类搜索分页
        public ActionResult PagingByCategories(string[] categories, int? page = 1, int? size = 10)
        {
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;

            categories = categories ?? new string[0];
            var model = _allProducts.Where(m => categories.Contains(m.Category))
               .ToPagedList(_currentPage, _pageSize);
            ViewBag.AllCategories = _allCategories;
            ViewBag.SelectedCategories = categories;
            return View(model);
        }

        //使用路由名称分页
        public ActionResult PagingByRoute()
        {
            return View();
        }

        //使用路由名称分页，为了模拟这种特殊的需求，所以使用了特殊的方式
        public ActionResult ShowPagingByRoute(int? page = 1, int? size = 10)
        {
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;

            var model = _allProducts.ToPagedList(_currentPage, _pageSize);
            return PartialView("_PartialPagingByRoute", model);
        }

        //ajax分页
        public ActionResult Ajax()
        {
            return View();
        }

        //ajax分页
        public ActionResult AjaxPage(int? page = 1, int? size = 10)
        {
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;

            var model = _allProducts.ToPagedList(_currentPage, _pageSize);
            return PartialView("_ProductGrid", model);
        }

        //自定义模版
        public ActionResult Bootstrap(int? page = 1, int? size = 10)
        {
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;

            var model = _allProducts.ToPagedList(_currentPage, _pageSize);
            return View(model);
        }

        //自定义页码路由键值对
        public ActionResult CustomPageRouteValueKey(SearchModel search)
        {
            if (search.Page != null) _currentPage = (int)search.Page;
            var model = _allProducts.ToPagedList(_currentPage, _pageSize);
            return View(model);
        }
    }
}