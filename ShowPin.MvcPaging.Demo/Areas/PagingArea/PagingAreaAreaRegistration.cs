﻿using System.Web.Mvc;

namespace ShowPin.MvcPaging.Demo.Areas.PagingArea
{
    public class PagingAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PagingArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PagingArea_default",
                "PagingArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { "ShowPin.MvcPaging.Demo.Areas.PagingArea.Controllers" }
            );
        }
    }
}