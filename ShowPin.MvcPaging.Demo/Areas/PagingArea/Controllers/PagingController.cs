﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShowPin.MvcPaging.Demo.Models;

namespace ShowPin.MvcPaging.Demo.Areas.PagingArea.Controllers
{
    public class PagingController : Controller
    {
        int _currentPage = 1;
        int _pageSize = 10;
        private readonly string[] _allCategories = { "吃的", "穿的", "用的" };
        private readonly IList<Product> _allProducts = new List<Product>();

        public PagingController()
        {
            InitializeProducts();
        }

        //简单初始化产品集合
        private void InitializeProducts()
        {
            for (var i = 0; i < 122; i++)
            {
                var product = new Product
                {
                    Name = "产品 " + (i + 1)
                };
                var categoryIndex = i % 4;
                if (categoryIndex > 2)
                {
                    categoryIndex = categoryIndex - 3;
                }
                product.Category = _allCategories[categoryIndex];
                _allProducts.Add(product);
            }
        }
        public ActionResult Index(int? page = 1, int? size = 10)
        {
            _currentPage = page.HasValue ? page.Value : _currentPage;
            _pageSize = size.HasValue ? size.Value : _pageSize;

            var model = _allProducts.ToPagedList(_currentPage, _pageSize);
            return View(model);
        }
    }
}