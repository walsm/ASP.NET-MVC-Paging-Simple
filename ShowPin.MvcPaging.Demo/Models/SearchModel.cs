﻿namespace ShowPin.MvcPaging.Demo.Models
{
    public class SearchModel
    {
        public int? Page { get; set; }

        public SearchModel()
        {
            Page = 1;
        }
    }
}